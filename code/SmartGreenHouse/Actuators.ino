void controlPumps() {
  updateTimeStamp(14); //update current time as starting point in case of reset

  if (pumpmode == 0)
  {

  }
  else if (pumpmode == 1 || pumpmode == 2) //fixed settings
  {
    if (pumpmode == 1) // add OR condition to check for failed soil moisture
    {
      unsigned long current_time = readTimeStamp(14); //get current time

      //get time of last watering
      unsigned long last_watering_pump1 = readTimeStamp(6);
      unsigned long last_watering_pump2 = readTimeStamp(10);

      if (current_time - last_watering_pump1 > 1000 * 86400 * (7 / waterpump1_days) ) //if x days have passend since last watering
      {
        water(0, waterpump1_amount); //water
      }

      if (current_time - last_watering_pump2 > 1000 * 86400 * (7 / waterpump2_days) ) //if x days have passend since last watering
      {
        water(1, waterpump2_amount); //water
      }
    }
  }
  delay(100);
  io.run();
}


//update settings for water pump 1
void Waterpump1_handleMessage(AdafruitIO_Data *data) {

  Serial.println("Water pump 1:");

  updateVal(&waterpump1_current_val, data, 0);
  updateVal(&waterpump1_current_val, data, 2);
  updateVal(&waterpump1_current_val, data, 4);

}

//update settings for water pump 2
void Waterpump2_handleMessage(AdafruitIO_Data *data) {

  Serial.println("Water pump 2:");

  updateVal(&waterpump2_current_val, data, 1);
  updateVal(&waterpump2_days, data, 3);
  updateVal(&waterpump2_amount, data, 5);

}

//update an integer value for a waterpump in EEPROM
void updateVal(int* old, AdafruitIO_Data* d, byte adr)
{
  int new_water_val = d->toInt(); //convert data to integer

  if (new_water_val != *old) //if the value has changed
  {
    byte write_value = (byte) new_water_val; //convert data to bytes
    EEPROM.write(adr, write_value); //write it at the given adress in EEPROM
    EEPROM.commit();

    String waterpump_name = "Updated Water Pump " + String(adr + 1);
    printValue(waterpump_name, new_water_val, "%");
  }
}

void water (int pump, int amount) //activate one or both water pumps
{
  int pump_pin;
  int pump_pin2 = -1;  

  if (pump == 0) //pump 1 
  {
    Serial.println("Pump 1:");
    pump_pin = PUMP_PIN_1;
    //waterpump1_time = updateTimeStamp(6); //update watering time stamp for pump 1
    updateTimeStamp(6); //update watering time stamp for pump 1
  }
  else if (pump == 1) //pump 2 
  {
    Serial.println("Pump 2:");
    pump_pin = PUMP_PIN_2;
    //waterpump2_time = updateTimeStamp(10); //update watering time stamp for pump 2
    updateTimeStamp(10); //update watering time stamp for pump 2
  }
  else if (pump == 2) //both
  {
    Serial.println("Pump 1 and Pump 2:"); 
    pump_pin = PUMP_PIN_1;
    pump_pin2 = PUMP_PIN_2;
    //waterpump1_time = updateTimeStamp(6); //update watering time stamp for pump 1
    //waterpump2_time = updateTimeStamp(10); //update watering time stamp for pump 2
    updateTimeStamp(6); //update watering time stamp for pump 1
    updateTimeStamp(10); //update watering time stamp for pump 2
  }

 // digitalWrite(pump_pin,HIGH); //activate the first pump

  if (pump_pin2 > -1) //if selected
  {
    //digitalWrite(pump_pin2,HIGH); //also activate the second pump
  }

  if (amount == 1)
  {
    Serial.println("Low ammount of water");
  }
  else if (amount == 2)
  {
    Serial.println("Medium ammount of water");
  }
  else if (amount == 3)
  {
    Serial.println("High ammount of water");
  }

  Serial.println("Watering now");
  
  delay(1000 *(1 + amount*2)); //let the pumps run for a given time dependent on the chosen amount of water

   digitalWrite(pump_pin,LOW); //deactivate the first pump

  if (pump_pin2 > -1) //if selected
  {
    digitalWrite(pump_pin2,LOW); //also deactivate the second pump
  }
}
