//update the long value of a time stamp in EEPROM
void updateTimeStamp(int adr)
{
  unsigned long l = millis() + offset; //get current time + offset (last timestamp before reset/restart)

  Serial.print("save time: ");

  Serial.println(l);

  byte bytes[4];

  //generate byte array from long
  bytes[0] = (l & 0xFF);
  bytes[1] = ((l >> 8) & 0xFF);
  bytes[2] = ((l >> 16) & 0xFF);
  bytes[3] = ((l >> 24) & 0xFF);

  //write the byte values at the given adress in EEPROM
  EEPROM.write(adr, bytes[0]);
  EEPROM.write(adr + 1, bytes[1]);
  EEPROM.write(adr + 2, bytes[2]);
  EEPROM.write(adr + 3, bytes[3]);
  
  EEPROM.commit();
}

//read a time stamp from EEPROM
uint32_t readTimeStamp(int adr)
{
  byte bytes[4];
  //load a byte array with values from the given adress
  bytes[0] = EEPROM.read(adr);
  bytes[1] = EEPROM.read(adr + 1);
  bytes[2] = EEPROM.read(adr + 2);
  bytes[3] = EEPROM.read(adr + 3);
  //return the byte array as long
  return ((bytes[0] << 0) & 0xFF) + ((bytes[1] << 8) & 0xFFFF) + ((bytes[2] << 16) & 0xFFFFFF) + ((bytes[3] << 24) & 0xFFFFFFFF);
}

void updateUInt16EEPROM (int adr, int value){
    Serial.print("Updated EEPROM with new uint16_t value: ");
    Serial.print(value);
    Serial.print(" at address: ");
    Serial.println(adr);
    byte bytes[2];
    bytes[0] = (value & 0xFF);
    bytes[1] = ((value >> 8) & 0xFF);
    EEPROM.write(adr, bytes[0]);
    EEPROM.write(adr + 1, bytes[1]);
}

uint16_t readUInt16EEPROM (int adr){
    byte bytes[2];
    bytes[0] = EEPROM.read(adr);
    bytes[1] = EEPROM.read(adr + 1);
    uint16_t value = ((bytes[0] << 0) & 0xFF) + ((bytes[1] << 8) & 0xFFFF);
    Serial.print("red uint16_t value from EEPROM: ");
    Serial.print(value);
    Serial.print(" at address: ");
    Serial.println(adr);
    return value;
}
