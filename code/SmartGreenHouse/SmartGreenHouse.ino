///************************** Configuration ***********************************/
#include "config.h"
#include "AdafruitIO_WiFi.h"
AdafruitIO_WiFi io(IO_USERNAME, IO_KEY, WIFI_SSID, WIFI_PASS);

/************************ EEPROM Init *******************************/
#include <EEPROM.h>
#define EEPROM_SIZE 22 //2 bytes sensor thresholds, 2 bytes weekdays, 2 bytes amount, 12 bytes timestamps (4 bytes each), 4 bytes last soil sensor values (2 bytes each)

/************************ Adafruit_Sensor Init *******************************/
#if  defined(USE_DHT) || defined(USE_BME280)
#include <Adafruit_Sensor.h>
// set up the 'temperature' and 'humidity' feeds
AdafruitIO_Feed *temperature = io.feed("temperature");
AdafruitIO_Feed *humidity = io.feed("humidity");
#endif

/************************ I2C Init *******************************/
#if defined(USE_BH1750) || defined(USE_BME280)
#include <Wire.h>
#endif

/************************ DHT11/22 Config *******************************/
#if defined(USE_DHT)
#include <DHT.h>
#include <DHT_U.h>

#define DHT_AVRG_COUNT 5

// create DHT22 instance
DHT_Unified dht(DATA_PIN, DHT22);
#endif

/************************ BME280 Temp & Humi Sensor *******************************/
#if defined(USE_BME280)
#include <Adafruit_BME280.h>
Adafruit_BME280 bme;
#endif

/************************ BH1750 Light Sensor *******************************/
#if defined(USE_BH1750)
#include <BH1750.h>
BH1750 lightMeter(0x23);
AdafruitIO_Feed *bh1750 = io.feed("bh1750");
#endif


/************************ Analog Soil Moisture Sensors *******************************/
AdafruitIO_Feed *soilMoistureFeeds[soil_sensor_number];
#if defined(USE_MEDIAN_FILTER)
  #include <RunningMedian.h>
  RunningMedian soil_sensor_samples = RunningMedian(ANALOG_FILTER_COUNT);
#endif
#if defined(USE_EXPONENTIAL_FILTER)
  #include "Filter.h"
  ExponentialFilter<int> ADCFilter(10,2000);
#endif

/************************ Light Sensor *******************************/
#if defined(USE_LDR)
AdafruitIO_Feed *light = io.feed("light");
#endif

/************************ Water Pumps *******************************/
AdafruitIO_Feed *watering_mode = io.feed("watering_mode");

AdafruitIO_Feed *waterpump1 = io.feed("waterpump1");
AdafruitIO_Feed *waterpump2 = io.feed("waterpump2");

AdafruitIO_Feed *days1 = io.feed("days1");
AdafruitIO_Feed *days2 = io.feed("days2");

AdafruitIO_Feed *amount1 = io.feed("amount1");
AdafruitIO_Feed *amount2 = io.feed("amount2");

int waterpump1_current_val;
int waterpump2_current_val;

int waterpump1_days;
int waterpump2_days;

int waterpump1_amount;
int waterpump2_amount;

unsigned long waterpump1_time;
unsigned long waterpump2_time;

unsigned long offset;

int pumpmode; //0 = sensor only, 1 = fixed settings, 2 = sensor with fixed settings as fallback

void setup() {
  // start the serial connection
  Serial.begin(115200);

  EEPROM.begin(EEPROM_SIZE); //initialize the EEPROM with the correct size

  //initialize variables with values stored in EEPROM

  waterpump1_current_val = EEPROM.read(0);
  waterpump2_current_val = EEPROM.read(1);

  waterpump1_days = EEPROM.read(2);
  waterpump2_days = EEPROM.read(3);

  waterpump1_amount = EEPROM.read(4);
  waterpump2_amount = EEPROM.read(5);

  offset = readTimeStamp(14); //get the last saved (before reset/restart) timestamp from EEPROM

  pinMode(LED_PIN_1, OUTPUT);
  pinMode(LED_PIN_2, OUTPUT);
  pinMode(PUMP_PIN_1, OUTPUT);

  digitalWrite(LED_PIN_2, HIGH); //Turn the LED on at the beginning of the code

  // wait for serial monitor to open
  while (!Serial);
  Serial.println("Adafruit IO + SmartGreenHouse");

  initSoilMoistureSensors();

#if defined(USE_DHT)
  // initialize dht22
  dht.begin();
#endif

#if  defined(USE_BH1750) || defined(USE_BME280)
  // Initialize the I2C bus
  Wire.begin(SDA_PIN, SCL_PIN);
#endif

#if defined(USE_BH1750)
  // Start the BH1750 Sensor
  lightMeter.begin();
#endif

#if defined(USE_BME280)
  bme.begin(0x76);
  //Only measure Temperature and Humidity
  bme.setSampling(Adafruit_BME280::MODE_FORCED,
                  Adafruit_BME280::SAMPLING_X8,   // temperature
                  Adafruit_BME280::SAMPLING_NONE, // pressure
                  Adafruit_BME280::SAMPLING_X8,   // humidity
                  Adafruit_BME280::FILTER_OFF );
#endif

  // set up a message handler for the 'waterpump1' feed.
  waterpump1->onMessage(Waterpump1_handleMessage);
  waterpump2->onMessage(Waterpump2_handleMessage);

  // connect to the Adafruit IO Library
  connectAIO();

  // check for latest message
  waterpump1->get();
  waterpump2->get();

  // set up and write to the feeds
  feedWrite();
  controlPumps();

  // let's go back to sleep for DEEPSLEEP_DURATION seconds...
  Serial.println("sleeping...");
  //ESP.deepSleep(DEEPSLEEP_DURATION); // for ESP8266

  esp_sleep_enable_timer_wakeup(DEEPSLEEP_DURATION); //for ESP32
  Serial.flush();

  digitalWrite(LED_PIN_2, LOW); //Turn the LED off at the end of the code

  esp_deep_sleep_start(); //for ESP32
}

void loop() {
}

void connectAIO() {
  Serial.println("Connecting to Adafruit IO...");
  io.connect();

  // wait for a connection
  while (io.status() < AIO_CONNECTED) {
    if (millis() >= ERROR_RESET_DURATION) { //prevente code run forever when no connection could be made
      io.wifi_disconnect();
      Serial.println();
      Serial.println("Restart ESP now!");
      Serial.flush();
      delay(5000);
      ESP.restart();
    }
    Serial.print(".");
    delay(200);
    digitalWrite(LED_PIN_1, HIGH);
    delay(200);
    digitalWrite(LED_PIN_1, LOW);
  }

  // we are connected
  Serial.println();
  Serial.println(io.statusText());
}
