void feedWrite() {
  runSensors();
  delay(100);
  io.run();
}

void runSensors() {
#if defined(USE_DHT)
  dhtSensor();
#endif

#if defined(USE_BME280)
  bme280Sensor();
#endif

#if defined(USE_BH1750)
  bh1750Sensor();
#endif

  moistureSensors();

#if defined(USE_LDR)
  lightSensor();
#endif
}

#if defined(USE_DHT)
void dhtSensor() {
  sensors_event_t event;

  float tempSum = 0.0;
  for (int i = 0; i < DHT_AVRG_COUNT; i++) {
    dht.temperature().getEvent(&event);
    tempSum += event.temperature;
    delay(3000);
  }

  float dhtTemperature = tempSum / (float)DHT_AVRG_COUNT;
  printValue("Temperature", dhtTemperature, "°C");

  // save celsius to Adafruit IO
  temperature->save(dhtTemperature);


  float humiSum = 0.0;
  for (int i = 0; i < DHT_AVRG_COUNT; i++) {
    dht.humidity().getEvent(&event);
    humiSum += event.relative_humidity;
    delay(3000);
  }

  float dhtHumidity = humiSum / (float)DHT_AVRG_COUNT;
  printValue("Humidity", dhtHumidity, "%");

  // save humidity to Adafruit IO
  humidity->save(dhtHumidity);
}
#endif

#if defined(USE_BH1750)
void bh1750Sensor() {
  float lux = lightMeter.readLightLevel();
  printValue("Light", lux, "lux");
  bh1750->save(lux);
}
#endif

#if defined(USE_BME280)
void bme280Sensor() {
  bme.takeForcedMeasurement();

  float bmeTemperature = bme.readTemperature();
  printValue("Temperature", bmeTemperature, "°C");
  temperature->save(bmeTemperature);

  float bmeHumidity = bme.readHumidity();
  printValue("humidity", bmeHumidity, "%");
  humidity->save(bmeHumidity);
}
#endif

void initSoilMoistureSensors() {
  for (int i = 0; i < soil_sensor_number; i++) {
    String feedname = "moisture" + String(i + 1);
    soilMoistureFeeds[i] = io.feed(feedname.c_str());
    last_soil_sensor_values[i] = readUInt16EEPROM(last_soil_sensor_addr[i]);
  }
}

void moistureSensors() {
  for (int j = 0; j < soil_sensor_number; j++) {
    
    #if defined(USE_MEDIAN_FILTER)
      soil_sensor_samples.clear();
    #endif
    
    #if defined(USE_EXPONENTIAL_FILTER)
      ADCFilter.SetCurrent(analogRead(soil_sensor_pins[j]));
    #endif

    #ifndef USE_MEDIAN_FILTER || USE_EXPONENTIAL_FILTER
      uint32_t analogSum = 0;
    #endif
    
    for (int i = 0; i < ANALOG_FILTER_COUNT; i++) {
        #if defined(USE_MEDIAN_FILTER)
          soil_sensor_samples.add(analogRead(soil_sensor_pins[j]));
        #endif
        #if defined(USE_EXPONENTIAL_FILTER)
           ADCFilter.Filter(analogRead(soil_sensor_pins[j]));
        #endif
        #ifndef USE_MEDIAN_FILTER || USE_EXPONENTIAL_FILTER
          analogSum += analogRead(soil_sensor_pins[j]);
        #endif
        delay(10);
    }

    int analogValue;
    String analog_name;
    
    #if defined(USE_MEDIAN_FILTER)
      analogValue = soil_sensor_samples.getMedian();
      analog_name = "Analog Moisture Median " + String(j+1);
      printValue(analog_name, analogValue, "");
    #endif

    #if defined(USE_EXPONENTIAL_FILTER)
      analogValue = ADCFilter.Current();
      analog_name = "Analog Moisture Exponential " + String(j+1);
      printValue(analog_name, analogValue, "");
    #endif
    
    #ifndef USE_MEDIAN_FILTER || USE_EXPONENTIAL_FILTER
      analogValue = analogSum / ANALOG_FILTER_COUNT;
      analog_name = "Analog Moisture Mean " + String(j+1);
      printValue(analog_name, analogValue, "");
      //analogValue = constrain(analogValue, capacitiveSensorMinValue, capacitiveSensorMaxValue);
      //int soilMoisture = map(analogValue, capacitiveSensorMaxValue, capacitiveSensorMinValue, capacitiveSensorMinValue, capacitiveSensorMaxValue);
    #endif
    
    uint16_t soilMoisture = map(analogValue, capacitiveSensorMaxValue, capacitiveSensorMinValue, 0, 1000);
    float soilMoisture_float = (soilMoisture + last_soil_sensor_values[j]) / 20.0;

    String last_moisture_name = "Last Soil Moisture " + String(j + 1);
    printValue(last_moisture_name, last_soil_sensor_values[j], "");

    String new_moisture_name = "New Soil Moisture " + String(j + 1);
    printValue(new_moisture_name, soilMoisture, "");

    updateUInt16EEPROM(last_soil_sensor_addr[j], soilMoisture);

    
    //float soilMoisture_float = (float)soilMoisture / 10.0;
    String moisture_name = "Soil Moisture " + String(j + 1);
    printValue(moisture_name, soilMoisture_float, "%");
    
    soilMoistureFeeds[j]->save(soilMoisture_float);
  }
}


#if defined(USE_LDR)
void lightSensor() {
  uint32_t analogSum = 0;
  for (int i = 0; i < ANALOG_FILTER_COUNT; i++) {
    analogSum += analogRead(LIGHT_SENSOR_PIN);
    delay(1);
  }
  int analogValue = analogSum / ANALOG_FILTER_COUNT;
  analogValue = constrain(analogValue, lightSensorMinValue, lightSensorMaxValue);
  float brightness = map(analogValue, lightSensorMinValue, lightSensorMaxValue, 0, 100);
  printValue("Brightness", brightness, "%");
  light->save(brightness);
}
#endif
