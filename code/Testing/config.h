/************************ Sensor Selection  *******************************/

//#define USE_DHT // uncomment for the use of a DHT22 Sensor for Temperature and Humidity
//#define USE_LDR // uncomment for the use of a analog LDR (light dependent resistor)
#define USE_BH1750 // uncomment for the use of a BH1750 light sensor over I2C
#define USE_BME280 // uncomment for the use of a BME280 Sensor for Temperature and Humidity

/************************ Pin Definitions  *******************************/

#define LED_PIN_1 18 //for debug only: blinking while connecting to Adafruit IO
#define LED_PIN_2 19 //for debug only: only on while not sleeping
#define PUMP_PIN_1 26
#define PUMP_PIN_2 25  
#define BUTTON_PIN_1 16
#define BUTTON_PIN_2 27


int8_t soil_sensor_pins[] = {34, 35};
const int soil_sensor_number = (const int)(sizeof(soil_sensor_pins) / sizeof(soil_sensor_pins[0]));
#define capacitiveSensorMinValue 0//1070 //mininum Value the sensor can reach
#define capacitiveSensorMaxValue 4095//2690 //maximum Value the sensor can reach


#define DHT_DATA_PIN 14 //Pin for DHT Data Line

#define LIGHT_SENSOR_PIN 35 //Light Sensor Pin for LDR
#define lightSensorMinValue 500 //mininum Value the sensor can reach
#define lightSensorMaxValue 4095 //maximum Value the sensor can reach

#define ANALOG_AVRG_COUNT 10 //How often the analog measurements should be taken (Soil Moisture and LDR)

#define SDA_PIN 21 //16 //Serial Data Pin for I2C
#define SCL_PIN 22 //17 // Serial Clock Pin for I2C

/************************ Deep Sleep and Time Config  *******************************/
#define DEEPSLEEP_DURATION 5*60e6 // 5 minutes * 60 seconds * 10^6 (conversion to microseconds)
#define ERROR_RESET_DURATION 30e3 // if the uptime is longer than 1 minute the esp will reset (prevent stuck in connection..)
