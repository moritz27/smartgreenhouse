void buttonDetection(){
  button_state_1 = digitalRead(BUTTON_PIN_1);
  if(button_state_1 == LOW && last_button_state_1 == HIGH){
    last_button_state_1 = LOW;
    digitalWrite(PUMP_PIN_1, HIGH);
    //Serial.println("Button 1 pressed");
  }
  if(button_state_1 == HIGH && last_button_state_1 == LOW){
    last_button_state_1 = HIGH;
    digitalWrite(PUMP_PIN_1, LOW);
    //Serial.println("Button 1 released");
  }

  button_state_2 = digitalRead(BUTTON_PIN_2);
  if(button_state_2 == LOW && last_button_state_2 == HIGH){
    last_button_state_2 = LOW;
    digitalWrite(PUMP_PIN_2, HIGH);
    //Serial.println("Button 2 pressed");
  }
  if(button_state_2 == HIGH && last_button_state_2 == LOW){
    last_button_state_2 = HIGH;
    digitalWrite(PUMP_PIN_2, LOW);
    //Serial.println("Button 2 released");
  }
}
