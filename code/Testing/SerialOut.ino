void printValue(String valueName, float value, String valueUnit){
  Serial.print(valueName);
  Serial.print(": ");
  Serial.print(value);
  Serial.print(" ");
  Serial.println(valueUnit);
}
