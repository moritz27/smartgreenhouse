/************************** Configuration ***********************************/
#include "config.h"

/************************ Adafruit_Sensor Init *******************************/
#if  defined(USE_DHT) || defined(USE_BME280)
  #include <Adafruit_Sensor.h>
#endif

/************************ I2C Init *******************************/
#if defined(USE_BH1750) || defined(USE_BME280)
  #include <Wire.h>
#endif

/************************ DHT11/22 Config *******************************/
#if defined(USE_DHT)
  #include <DHT.h>
  #include <DHT_U.h>
 
  #define DHT_AVRG_COUNT 5
  
  // create DHT22 instance
  DHT_Unified dht(DATA_PIN, DHT22);
#endif

/************************ BME280 Temp & Humi Sensor *******************************/
#if defined(USE_BME280)
  #include <Adafruit_BME280.h>
  Adafruit_BME280 bme;
#endif

/************************ BH1750 Light Sensor *******************************/
#if defined(USE_BH1750)
  #include <BH1750.h>
  BH1750 lightMeter(0x23);
#endif

/************************ Buttons *******************************/

byte button_state_1 = HIGH;
byte last_button_state_1 = HIGH;
byte button_state_2 = HIGH;
byte last_button_state_2 = HIGH;


void setup() {
  Serial.begin(115200);

  pinMode(PUMP_PIN_1, OUTPUT);
  pinMode(PUMP_PIN_2, OUTPUT);
  digitalWrite(PUMP_PIN_1, LOW);
  digitalWrite(PUMP_PIN_2, LOW);

  pinMode(BUTTON_PIN_1, INPUT_PULLUP);
  pinMode(BUTTON_PIN_2, INPUT_PULLUP);

  #if defined(USE_DHT)
    // initialize dht22
    dht.begin();
  #endif

  #if  defined(USE_BH1750) || defined(USE_BME280)
    // Initialize the I2C bus
    Wire.begin(SDA_PIN, SCL_PIN);
  #endif
    
  #if defined(USE_BH1750)
    // Start the BH1750 Sensor
    lightMeter.begin();  
  #endif
    
  #if defined(USE_BME280)
    bme.begin(0x76);
    //Only measure Temperature and Humidity
    bme.setSampling(Adafruit_BME280::MODE_FORCED,
                    Adafruit_BME280::SAMPLING_X8,   // temperature
                    Adafruit_BME280::SAMPLING_NONE, // pressure
                    Adafruit_BME280::SAMPLING_X8,   // humidity
                    Adafruit_BME280::FILTER_OFF );
  #endif

}

void loop() {
  buttonDetection();
  runSensors();
  delay(1000);
}
