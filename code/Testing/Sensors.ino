void runSensors() {
#if defined(USE_DHT)
  dhtSensor();
#endif

#if defined(USE_BME280)
  bme280Sensor();
#endif

#if defined(USE_BH1750)
  bh1750Sensor();
#endif

  moistureSensors();

#if defined(USE_LDR)
  lightSensor();
#endif

Serial.println("************************");
}

#if defined(USE_DHT)
void dhtSensor() {
  sensors_event_t event;

  float tempSum = 0.0;
  for (int i = 0; i < DHT_AVRG_COUNT; i++) {
    dht.temperature().getEvent(&event);
    tempSum += event.temperature;
    delay(3000);
  }

  float dhtTemperature = tempSum / (float)DHT_AVRG_COUNT;
  printValue("Temperature", dhtTemperature, "°C");

  float humiSum = 0.0;
  for (int i = 0; i < DHT_AVRG_COUNT; i++) {
    dht.humidity().getEvent(&event);
    humiSum += event.relative_humidity;
    delay(3000);
  }
  float dhtHumidity = humiSum / (float)DHT_AVRG_COUNT;
  printValue("Humidity", dhtHumidity, "%");
}
#endif

#if defined(USE_BH1750)
void bh1750Sensor() {
  float lux = lightMeter.readLightLevel();
  printValue("Light", lux, "lux");
}
#endif

#if defined(USE_BME280)
void bme280Sensor() {
  bme.takeForcedMeasurement();

  float bmeTemperature = bme.readTemperature();
  printValue("Temperature", bmeTemperature, "°C");

  float bmeHumidity = bme.readHumidity();
  printValue("humidity", bmeHumidity, "%");

}
#endif


void moistureSensors() {
  for (int j = 0; j < soil_sensor_number; j++) {
    uint32_t analogSum = 0;
    for (int i = 0; i < ANALOG_AVRG_COUNT; i++) {
      analogSum += analogRead(soil_sensor_pins[j]);
      delay(10);
    }
    int analogValue = analogSum / ANALOG_AVRG_COUNT;
    //String analog_name = "analog_moisture" + String(j+1);
    //printValue(analog_name, analogValue, "");
    //analogValue = constrain(analogValue, capacitiveSensorMinValue, capacitiveSensorMaxValue);
    int soilMoisture = map(analogValue, capacitiveSensorMaxValue, capacitiveSensorMinValue, 0, 100);
    //int soilMoisture = map(analogValue, capacitiveSensorMaxValue, capacitiveSensorMinValue, capacitiveSensorMinValue, capacitiveSensorMaxValue);
    String moisture_name = "moisture" + String(j+1);
    printValue(moisture_name, soilMoisture, "%");
  }
}


#if defined(USE_LDR)
void lightSensor() {
  uint32_t analogSum = 0;
  for (int i = 0; i < ANALOG_AVRG_COUNT; i++) {
    analogSum += analogRead(LIGHT_SENSOR_PIN);
    delay(1);
  }
  int analogValue = analogSum / ANALOG_AVRG_COUNT;
  analogValue = constrain(analogValue, lightSensorMinValue, lightSensorMaxValue);
  float brightness = map(analogValue, lightSensorMinValue, lightSensorMaxValue, 0, 100);
  printValue("Brightness", brightness, "%");
}
#endif
