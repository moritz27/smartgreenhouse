/************************ Adafruit IO Config *******************************/
#define IO_USERNAME "yourAdafruitIOuser"
#define IO_KEY "yourAdafruitIOkey"

/******************************* WIFI **************************************/
#define WIFI_SSID ""yourSSID"
#define WIFI_PASS "yourPassword"