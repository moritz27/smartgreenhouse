# SmartGreenHouse

IoT-Project to realize a smart green house with common infrastructure

## Credentials

To make this project work correct you have to create a file called "credentials.h" with your personal data for AdafruitIO and your Wifi network.
The file needs to be in the folder "code/SmartGreenHouse".

You can use the template called "testCredentials.h".

Take care when pushing something to this repo to keep your credentials safe!